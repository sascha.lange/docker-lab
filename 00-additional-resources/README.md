# Description

This README contains some useful informations / tools that could be helpful for you.

## [Docker Engine](https://www.docker.com/)

Official website of Docker

## [Docker Documentation Page](https://docs.docker.com/)

Dockers documentation

## [lazydocker](https://github.com/jesseduffield/lazydocker)

A project that provides an interactive menu to work with containers. It serves nummerous tasks like reading logs, stopping container and so on.
