# Description

This lab is designed to describe the fundamental basics of docker and how to use it. It includes exercises that can be replayed on your computer. This repository contains the directories **basic-commands**, **execises** and **solutions**. The purpose of each directory is described below. Each directory contains at least a README.md file with informations and/or exercises.

## [00-additional-resources](00-additional-resources/README.md)

A collection of useful informations, links and tools.

## [01-basic-commands](01-basic-commands/README.md)

A summary of the most important commands and paramaters of the docker cli.

## [02-exercises](02-exercises/README.md)

Contains execises to get familiar with docker. It should provide enough information to compose your own docker images and run them.

## [03-solutions](03-solutions/README.md)

Contains examples of possible solutions.

---

## Prerequisites

Docker must be installed on your local computer. Follow the [official instructions](https://docs.docker.com/get-docker/) to install docker for your OS of choice.

---

## What is Docker and why do we need it

Docker is an open-source engine that automates the deployment of applications into containers. It adds an application deployment engine on top of a isolated container exectution environment.

Docker is fast and allows you to Dockerize your applications in minutes. With Docker, Developers care about their application running inside containers, and operations care about managing them.

Docker is designed to enhance consistencyby ensuring the environment in which your developers write code matches the environment into which your applications are deployed. This reduces the risk of "worked in dev, now an ops problem."

Docker is a client-server application. The docker client talks to the Docker server or daemon, which does all the work. Sometimes the Docker daemon is also called the Docker Engine.

See the image below to get an idea of the different docker componets, that are involved when using docker commands.

![docker components](./images/docker_components.jpg)

---

## Docker Hub

A lot of opensource projects and also communities maintaining predefined docker images. Often they are published on the official docker registriy called [Docker Hub](https://hub.docker.com/). Docker Hub is always a good starting point to search for available images that suit your need. But be aware some project do not publish on Docker Hub and using their own registry to publish images.

---

### Images

Docker images are the building block of the Docker world. You launch your containers from images. Images are the "build" part of the Docker's life cycle. They are a layered format, using Union file systems, that are built ste-by-step using a series of instructions. For example:

- Add a file
- Run a command
- Open a port

Images are highly portable and can be shared , stored and updated. Images are versioned by using diffrent and be referenced by using tags.

---

### Registries

Docker stores the images you build in registries. There are public and private registries. Docker, Inc., operates the public registry for images, called the [Docker Hub](https://hub.docker.com/)

The Docker Hub counts over 10.000 images that other people have built and shared. The most popular open-source projects are releasing images on Docker Hub.

---

### Containers

You cant think about images as the building or packaging aspect of Docker and the containers as the running or execution aspect of Docker. A Docker container is:

- An image format
- A set of standard operations
- An execution environment

---

## What docker is not

Docker is not a virtualization platform like VMWare or virtualbox. Docker containers are always using the OS kernel of the host system. They are isolated from each and can be limited in resource usage and access by using Linux kernel mechanisms. While a virtualization platform always emulates a complete hardware architecture on which you can install you own OS.

![hypervisor vs container](images/hypervisor_vs_container.jpg)
