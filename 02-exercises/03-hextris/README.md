# Description

In this exercise you will build an image based on the [official nginx image](https://hub.docker.com/_/nginx). To keep the image small use the variant with alpine.

---

1. Create a Dockerfile that includes the correct docker image with the **FROM** instruction.

2. Copy the source code of the application to the correct location within the image. In this case this is **/usr/share/nginx/html**. The source code of our application is part of this directory and  is called **hextris**.

3. Build the docker image with the name hextris.

4. Run the image with docker run command and expose the webserver on port 8080.

5. Access the web app on http://localhost:8080
