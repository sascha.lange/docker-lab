# Description

In this exercise you will create your own custom docker image. Docker setups are described in a so called Dockerfile. A Dockerfile contains a sequence of instructions that are executed to build your custom image. These instructions include **CMD, ENTRYPOINT, ADD, COPY, VOLUME, WORKDIR, USER, ONBUILD, LABEL, STOPSIGNAL, ARG, SHELL, HEALTHCHECK and ENV.** For a complete list of available Dockerfile instructions check the [official Dockerfile reference](https://docs.docker.com/engine/reference/builder/).

---

1. Create your first Dockerfile with the content below

    ```
    FROM ubuntu
    LABEL maintainer="hello@world.com"
    ```

2. Build a docker image

    ```
    docker build -t example .
    docker tag example:latest example:v1
    ```

3. Install a web server and run our container. Add the following lines to the end of your Dockerfile.

    ```
    ARG DEBIAN_FRONTEND=noninteractive
    RUN apt-get update
    RUN apt-get install -y nginx

    COPY config/nginx.conf /etc/nginx/nginx.conf
    COPY content/ /var/www/html/

    EXPOSE 80/tcp

    CMD ["nginx", "-g", "daemon off;"]
    ```

4. Run your image

    ```
    docker run --rm -p 8080:80 example

    or

    docker run --rm -p 8080:80 example:v1
    ```

5. Docker layers: A best practise for building images is to keep them as small as possible and also minimize the amount of layers in an image. Every line with an instruction inside of your Dockerfile will create an additional layer. Currently the size of our Docker image should be about 160 MB. Check that with the image parameter.

    ```
    docker image ls

    or

    docker images
    ```

6. Now try to shrink the image by executing some clean up operations in your Dockerfile. Include the two commands below in your Dockerfile so that they are executed during the build phase.

    ```
    apt-get autoclean
    rm -rf /var/lib/apt/lists
    ```

7. Now verify the image size again. You will notice that the image size did not change. Do you have any clue why?
