# Description

In this first exercise you will start your first container and get familiar with some of the basic docker commands.

## Verify Docker is running

```
docker info
```

## Pull images

Images can be searched on the command line with:

```
docker search name
```

or you start searching directly on [Docker Hub](https://hub.docker.com/).

To download an image use the pull command. If you secify only a name without a _tag_ docker will try to download always latest.

So the command **docker pull ubuntu** will translate into **docker pull ubuntu:latest**. Before downloading the Docker daemon will try to find an image with this exact _name_ and _tag_ on the local disk drive. Only if it is not present a download from the registry is initiated.

Download the latest ubuntu image using docker pull and check if it present:

```
docker pull ubuntu

docker image ls
```

## Run docker

To run a docker container execute the docker run parameter with an "image name" as input. If the image is not present on your computer the Docker daemon will try to download it from the public Docker Hub registry.

1. Run an image of ubuntu

    ```
    docker run --rm ubuntu
    ```

2. Run the image with a valid unix command, for example the echo command

    ```
    docker run --rm ubuntu echo Hello World
    ```

3. The previous examples were just one time commands and the container exited right after our command finished. Let's find out why. Check the docker image with docker inspect too investigate what is executed during runtime. Most important are the CMD and ENTRYPOINT values in the Config section.

    ```
    docker inspect ubuntu
    ```

4. We can also run our images in an interactive mode with a tty attached to it. Run a docker container with an interactive terminal session. Run some commands and create a file.

    ```
    docker run --rm -ti ubuntu
    ```

5. Inside of your container session execute the commands below:

    ```
    echo "Hello" >> test.txt
    cat test.txt
    ```

6. Exit the container and start it again with the docker run command from exercise 4 and cat your file again.

    ```
    exit
    ```

    ```
    docker run --rm -ti ubuntu
    ```

    ```
    cat test.txt
    ```

7. Run a container in the background and check if it is running

    ```
    docker run --rm nginx -d
    docker ps
    ```

8. Check the resource usage of your container

    ```
    docker stats
    ```

9. Check for running processes inside your container

    ```
    docker top <Container ID>
    ```

10. Check your container logs

    ```
    docker logs -f <Container ID>
    ```

11. Stop your container

    ```
    docker kill <Container ID>
    ```

---

## Summary

In this exercise you learned how to download and execute a container, get start parameters of an image with docker inspect, retrieve logs and check resource usage of a running container. You also have seen that containers are immutable and date that is written inside of a container gets lost if the container restarts.
