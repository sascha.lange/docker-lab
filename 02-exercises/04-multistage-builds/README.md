# Description

This lab shows an example for aso called multistage build. Multistage builds are a common practice to create docker images that are as possible.

---

1. Build the docker image

    ```
    docker build -t multistage
    ```

2. Check the image sizes of the images we used

    ```
    docker image ls golang:1.14.12-alpine3.12
    docker images ls multistage
    ```

3. We can also run the image ;)

    ```
    docker run -p 8080:80 -ti multistage
    ```
