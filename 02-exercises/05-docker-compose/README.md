# Description

This chapter will give you a brief overview of Docker Compose and what it is used for. The chapter already includes a working docker-compose.yml that is used as an example.

Docker Compose allows you to run stacks of containers to represent application stacks, for example web server, application server and database server containers running together to server a specific application. The example in this chapter includes a wordpress stack which contains a wordpress and a database container.

Applications stacks are defined in a configuration file that is calles **docker-compose.yml**. For a complete reference please check the [compose-file reference](https://docs.docker.com/compose/compose-file/).

To start a stack Docker Compopse comes with it's own command called **docker-compose**. Some of the command options are similiar to the docker command. You can find a complete overview [here](https://docs.docker.com/compose/reference/).

Docker compose links containers so that they can communicate on network level using the service names as DNS server names within the containers. We will explore this feature by running the example stack.

---

1. Start the Docker Compose stack

    ```shell
    docker-compose up -d

    or

    docker-compose up
    ```

2. Check if the container is running

    ```shell
    docker-compose ps
    ```

3. Check for running processes within the conntainers

    ```shell
    docker-compose top
    ```

4. Stop the Docker Compose stack

    ```shell
    docker-compose down
    ```

---

**You can always use docker command to interact with containers that are created with docker-compose.**
