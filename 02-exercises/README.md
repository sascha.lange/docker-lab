# Exercise Index

---

[01 - Play with docker run](01-docker-run/README.md)

[02 - Build an image](02-docker-build)

[03 - Build your own hextris instance](03-hextris)

[04 - Multistage builds](04-multistage-builds)

[05 - Docker Compose in action](05-docker-compose)
