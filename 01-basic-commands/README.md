# Description

A brief description of basic docker commands. This list is not complete, you can always reffer to the official documentation or get available docker parameters directly from the CLI.

```shell
# Display available parameters
docker --help
```

The picture below illustrates a simple lifecycle of a docker container. It starts by pulling an image and creating a container by running the image. That creates a new instance of that image, which we call a container. We can interact with that instance by issuing commands like stop/start, pause/unpause and kill.

![container lifecycle](../images/container_lifecycle.jpg)

## Working with images

All of these commands have in common that they are used to create docker images, downloading or uploading them.

```shell
# Authenticate to a docker registry
# Only if authentication is required
docker login --username <username> --password-stdin <docker registry URL>

# Search Docker Hub for images
docker search <name>
```

```shell
# List local images
docker images
```

```shell
# List local images, same as docker images
docker image ls

# Delete a local image
docker image rm <image>
```

```shell
# Build an image from a Dockerfile in the same working directory
docker build -t <image name> .

# Create additional tag name for an image
docker tag <image name> <new image name>

# Push an image to a docker registry
# To specify the registry URL use the "docker tag" command to create an image with a name like <registry url>/<name>:<tag>
# Example name 211841842200.dkr.ecr.eu-central-1.amazonaws.com/athena-reverse-proxy:latest
docker push <image name>
```

```shell
# Download an image.
# If there is no registry URL specified, docker tries to download the image from Docker Hub.
# Images are only downloaded, if not already present.
docker pull <image name>
```

## Working with containers

Command related to start, stop and attach to containers

```shell
# Run an container an execute the entrypoint parameter.
docker run <image name>

# Run an container interactive and execute the command parameter.
# -ti provides an interactive terminal
# -rm removes the container when it stops from the process list
docker run --rm -ti <image name> <command>

# Run an image in background
docker run --rm -d <image name>

# Access a container interactive
docker exec -ti <container id> <command>

# Attach to an already available terminal session inside a container
docker attach <container id>

# Start, stop or restart a container that runs in background
docker start|stop|restart <container id>

# Pause or unpause a container that runs in background
docker pause|unpause <container id>

# Kill one or more running containers
docker kill <container id>
```

## Get informations of your containers

```shell
# Show statistics on host resource usage
docker stats

# Show running containers
docker ps

# Show running containers including stopped containers
docker ps -a

# Show process list from within a container
docker top <container id>

# Display container logs from stdout
docker logs <container id>

# Show container definition in json format
docker inspect <container id>
```
